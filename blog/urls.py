from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='blog-home'),
    path('about/', views.about, name='blog-about'),
    path('check/', views.about),
    path('export/', views.export, name='export'),
    path('populate_form/', views.populate_form, name='populate_form')
    
]

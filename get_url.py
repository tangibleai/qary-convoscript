
import json
import base64

def get_url(hosted_url, suggested_intent, suggested_bot_statement):
    obj = {"statename": suggested_intent, "botstatement": suggested_bot_statement}
    strobj = json.dumps(obj)
    message_bytes = strobj.encode('ascii')
    base64_bytes = base64.b64encode(message_bytes)
    base64_message = base64_bytes.decode('ascii')
    hosted_url = hosted_url.strip()
    if hosted_url[len(hosted_url) - 1] == "/":
        hosted_url = hosted_url[:len(hosted_url)-1]

    generated_url = hosted_url + "/populate_form/?obj="+ base64_message

    return generated_url

if __name__ == "__main__":
    generated_url = get_url("http://127.0.0.1:8000/", "how_are_you", "How are you doing?")
    print(generated_url)

